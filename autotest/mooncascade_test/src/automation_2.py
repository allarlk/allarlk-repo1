from selenium import webdriver
import time
from selenium.webdriver.common.alert import Alert

print("""
Test 2: 
1) Autotest will log in with existing user and
2) new training plan for biceps is created and saved
3) attempt to delete created workout plan, alert is first dismissed,
4) attempt to delete created workout plan, alert is accepted and workout plan is deleted
5) user is logged out and browser will be closed
""")

driver = webdriver.Chrome('..\drivers\chromedriver.exe')

driver.set_page_load_timeout(20)

## register with incorrect email
driver.get('https://www.gymwolf.com/staging/')
driver.find_element_by_xpath('//*[@id="main-menu"]/ul/li[5]/a').click()
driver.maximize_window()

time.sleep(2)

#insert credentials for loggingin in
email = 'allar.lauk@hotmail.com'
password = 'test123'

#insert email and password and log in
driver.find_element_by_name('email').click()
driver.find_element_by_xpath('//*[@id="login-front"]/div/div/div/div/div/form/div[1]/input').send_keys(str(email))
driver.find_element_by_xpath('//*[@id="login-front"]/div/div/div/div/div/form/div[2]/input').send_keys(str(password))
driver.find_element_by_xpath('//*[@id="login-front"]/div/div/div/div/div/form/div[4]/div/div/button').click()

#click new trainingplan
driver.find_element_by_xpath('/html/body/div[2]/div/div/div[2]/p/a').click()

#insert data to workout plan
driver.find_element_by_name('exercise_name[]').send_keys('Bicep Curl Lunge with Bowling Motion')
time.sleep(1)
driver.find_element_by_xpath('//*[@id="workout_form"]/div[2]/div[3]/div[2]/div[1]/input[1]').send_keys('50')
driver.find_element_by_xpath('//*[@id="workout_form"]/div[2]/div[3]/div[2]/div[1]/input[2]').send_keys('10')

driver.find_element_by_xpath('//*[@id="workout_form"]/div[2]/div[3]/div[2]/div[2]/input[1]').send_keys('60')
driver.find_element_by_xpath('//*[@id="workout_form"]/div[2]/div[3]/div[2]/div[2]/input[2]').send_keys('8')

driver.find_element_by_xpath('//*[@id="workout_form"]/div[2]/div[3]/div[2]/div[3]/input[1]').send_keys('70')
driver.find_element_by_xpath('//*[@id="workout_form"]/div[2]/div[3]/div[2]/div[3]/input[2]').send_keys('6')

driver.find_element_by_name('bodyweight').send_keys('75')
driver.find_element_by_name('notes').send_keys('Notes testing dadsad')

driver.find_element_by_xpath('//*[@id="workout_form"]/div[6]/div/div[1]/button').click()

#delete workout plan(dismissa alert)
driver.find_element_by_xpath('//*[@id="workout_form"]/div[5]/div/div[1]/a').click()
Alert(driver).dismiss()
time.sleep(1)
driver.refresh()

#delete workout plan(accept alert)
driver.find_element_by_xpath('//*[@id="workout_form"]/div[5]/div/div[1]/a').click()
Alert(driver).accept()


#log user out
driver.find_element_by_xpath('//*[@id="main-menu"]/ul/li[6]/a').click()
driver.find_element_by_xpath('//*[@id="main-menu"]/ul/li[6]/ul/li[8]/a').click()

driver.quit()

print('Test completed')


