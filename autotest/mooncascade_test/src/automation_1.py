from selenium import webdriver
import time

driver = webdriver.Chrome('..\drivers\chromedriver.exe')
#driver = webdriver.Firefox('..\drivers\chromedriver.exe')
#driver = webdriver.Ie('..\drivers\chromedriver.exe')

driver.set_page_load_timeout(10)

## register with incorrect email
driver.get('https://www.gymwolf.com/staging/')
driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[3]/div[3]/a').click()
driver.find_element_by_name('signup_email').send_keys('wat@incorrect_email_format.com')
time.sleep(2)

driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[3]/div[3]/form/div[2]/button').click()
el = driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[2]/div/div').text

print(
"""Test 1:
1) Autotest enters e-mail for registering in incorrect format,
2) 'Vigane e-maili aadress' error must be returned, and message is validated that it contains correct error message
3) that test closes incorrect e-mail error from close button and browser will be closed\n""")

## expected result value
er = str(
"""×
Vigane e-maili aadress""")

if el == er:
    print("Actual result:\n" + str(el) + "\n")
    print("Expected result:\n " + str(er) + "\n")
    print("Expected result matches actual result\n")
else:
    print('Test failed')
time.sleep(1)

print("Incorrect e-mail notifiation will be closed from close button.\n")
driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[2]/div/div/button').click()
print("Test completed")
time.sleep(2)
driver.quit()

